﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public static class Menu
    {
        public static string[] mainMenu = new string[] { "Перевод целых чисел в римскую СИ",
        "Перевод римских чисел в десятичную СИ",
        "Сложение в произвольной системе счисления",
        "Умножение в произвольной системе счисления",
        "Выход"};
    }

    class Program
    {

        static void Main(string[] args)
        {
            int index = 0;
            while (true)
            {
                SelectionHighlight(Menu.mainMenu, index);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < Menu.mainMenu.Length - 1)
                            index++;
                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                            index--;
                        break;
                    case ConsoleKey.Enter:
                        switch (index)
                        {
                            case 0:
                                Console.WriteLine("Введите целое число для перевода:");
                                ConsoleHelper.ClearScreen();
                                int number = int.Parse(Console.ReadLine());
                                Console.WriteLine(ConvertingToRoman.ExecuteConverting(number));
                                GetBack(Menu.mainMenu, index);
                                break;
                            case 1:
                                ConsoleHelper.ClearScreen();
                                Console.WriteLine("Введите число в римской СИ для перевода:");
                                ConvertingRomanToDecimal ob = new ConvertingRomanToDecimal();
                                string str = Console.ReadLine();
                                Console.WriteLine(ob.romanToDecimal(str));
                                GetBack(Menu.mainMenu, index);
                                break;
                            case 2:
                                ConsoleHelper.ClearScreen();
                                Addition addition = new();
                                addition.OutputAddition();
                                GetBack(Menu.mainMenu, index);
                                break;
                            //case 3:
                            //    ConsoleHelper.ClearScreen();
                            //    Addition multiplacate = new();
                            //    multiplacate.OutputMultiplacate();
                            //    GetBack(Menu.mainMenu, index);
                            //    break;
                            case 3:
                                Console.WriteLine("Вы выбрали выход из приложения");
                                return;
                        }
                        break;
                }
            }
        }

        static void GetBack(string[] items, int index)
        {
            string inputChar;
            do
            {
                Console.WriteLine("Введите b/back для возвращения в главное меню, либо любой другой символ для выхода из программы");
                inputChar = Console.ReadLine();
                var requiredChar = inputChar?.ToLower();
                if ((requiredChar == "b") || (requiredChar == "back"))
                    break;
                else
                    Environment.Exit(0);
            } while (true);
            ConsoleHelper.ClearScreen();
            SelectionHighlight(items, index);
        }

        static void SelectionHighlight(string[] items, int index)
        {
            ConsoleHelper.ClearScreen();
            Console.WriteLine("Приветствую вас в калькуляторе по теме <Системы счисления>, ниже вам представлен доступный функционал \n ");
            for (int i = 0; i < items.Length; i++)
            {
                if (i == index)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(items[i]);
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine();
        }
    }
}
