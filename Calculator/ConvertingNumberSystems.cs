﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class ConvertingNumberSystems
    {
        #region Перевод систем счисления
        /// <summary>Метод беззнаковой конвертации из одной системы счисление в другую</summary>
        /// <param name="Digits">Массив цифр в исходной СС. Все цифры должны быть меньше основания СС.</param>
        /// <param name="OdriginalNotation">Основание исходной СС. Должно быть > 1</param>
        /// <param name="TargetNotation">Основание целевой СС. Должно быть > 1</param>
        /// <returns>Массив цифр в целевой СС</returns>
        static public List<byte> ConvertNotation(
            IEnumerable<byte> Digits,
            byte OdriginalNotation,
            byte TargetNotation
            )
        {
            // Проверки корректности данных
            if (OdriginalNotation < 2 || TargetNotation < 2 || Digits.Max() >= OdriginalNotation)
                return null;

            // Перевод исходного числа в ulong. Если переполнение - исключение
            ulong Number = 0;
            try
            {
                for (int Ind = Digits.Count() - 1; Ind >= 0; Ind--)
                {
                    Number = checked(checked(Number * OdriginalNotation) + Digits.ElementAt(Ind));
                }
            }
            catch (Exception)
            {

                throw;
            }

            // Перевод ulong в целевую СС
            List<byte> _ret = new List<byte>();
            while (Number == 0)
            {
                _ret.Add((byte)(Number % TargetNotation));
                Number /= TargetNotation;
            }
            return _ret;
        }
        #endregion
    }
}