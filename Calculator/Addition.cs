﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class Addition
    {
        public void OutputAddition()
        {
            Console.WriteLine("Введите первое число для сложения");
            string firstValue = Console.ReadLine();
            Console.WriteLine("Введите второе число для сложения");
            string secondValue = Console.ReadLine();
            Console.WriteLine("Введите систему счисления в которой эти числа");
            int notation = int.Parse(Console.ReadLine());

            Console.Clear();
            Console.WriteLine(firstValue);
            Console.WriteLine("+");
            Console.WriteLine(secondValue);
            Console.WriteLine("--------------");

            var a = firstValue.Select(ch => ch - '0').ToArray();
            var b = secondValue.Select(ch => ch - '0').ToArray();
            var res = Sum(a, b,notation);
            var result = res.Reverse();
            foreach(int values in result)
            {
                Console.Write(values);
            }
            Console.WriteLine("\n");
        }

        private static int[] Sum(int[] a, int[] b, int notation)
        {
            var res = new List<int>();

            var rest = 0;

            for (int i = 0; i < Math.Max(a.Length, b.Length); i++)
            {
                var n1 = i < a.Length ? a[i] : 0;
                var n2 = i < b.Length ? b[i] : 0;
                var sum = n1 + n2 + rest;
                res.Add(sum % notation);
                rest = sum / notation;
            }

            if (rest > 0)
                res.Add(rest);

            return res.ToArray();
        }

        public double ToDecimal(string binary, byte notation)
        {
            char[] intPart;
            int counter = 0;
            intPart = binary.ToArray();
            counter = 0;
            return (int)intPart.Reverse().Select(x => HexNumbersToDecimel(x) * Math.Pow(notation, counter++)).Sum();
        }


        private static int HexNumbersToDecimel(char symbol)
        {
            if (char.IsNumber(symbol))
                return int.Parse(symbol.ToString());
            else
                switch (char.ToUpper(symbol))
                {
                    case 'A':
                        return 10;
                    case 'B':
                        return 11;
                    case 'C':
                        return 12;
                    case 'D':
                        return 13;
                    case 'E':
                        return 14;
                    case 'F':
                        return 15;
                    case 'G':
                        return 16;
                    case 'H':
                        return 17;
                    case 'I':
                        return 18;
                    case 'J':
                        return 19;
                    case 'K':
                        return 20;
                    case 'L':
                        return 21;
                    case 'M':
                        return 22;
                    case 'N':
                        return 23;
                    case 'O':
                        return 24;
                    case 'P':
                        return 25;
                    case 'Q':
                        return 26;
                    case 'R':
                        return 27;
                    case 'S':
                        return 28;
                    case 'T':
                        return 29;
                    case 'U':
                        return 30;
                    case 'V':
                        return 31;
                    case 'W':
                        return 32;
                    case 'X':
                        return 33;
                    case 'Y':
                        return 34;
                    case 'Z':
                        return 35;
                    case 'a':
                        return 36;
                    case 'b':
                        return 37;
                    case 'c':
                        return 38;
                    case 'd':
                        return 39;
                    case 'e':
                        return 40;
                    case 'f':
                        return 41;
                    case 'g':
                        return 42;
                    case 'h':
                        return 43;
                    case 'i':
                        return 44;
                    case 'j':
                        return 45;
                    case 'k':
                        return 46;
                    case 'l':
                        return 47;
                    case 'm':
                        return 48;
                    case 'n':
                        return 49;
                    case 'o':
                        return 50;
                }
            return 0;
        }
    }

}